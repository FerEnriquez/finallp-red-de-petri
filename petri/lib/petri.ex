defmodule Petri do
  
  ############################
  #         Parte 2         #
  ############################
  def read() do
    File.read!("log.txt")
    |>String.split("\n")
    |>Enum.chunk_every(1)
  end

  def doGraph() do
    matrix = [[1,0,0,0,1],
              [1,0,0,0,1],
              [1,0,0,0,1],
              [1,0,0,0,1],
              [1,0,0,0,1]]
  end

  def checkGraph() do
    input = read()
    # nodeBef(2)
    # nodeAft(2)
    matrix = doGraph()
    #a = yes | b = no 
    result = %{a: 0, b: 0}
    #La transicion, la posición indica cuál es la transicion que quieres
    t = Enum.at(matrix, 1)
    #El nodo, la posicion indica el nodo que quieres
    m = Enum.at(matrix, 0)
    Enum.reduce(input, {0, 0}, 
      fn input, conn -> 
        {m, conn} = Enum.reduce(input, {m, true}, 
          fn t, {num, condition} ->
            {node, num} = fire(net, m, t)
          if node == :ok do
            Map.get_and_update(result, :a , fn x -> x + 1 end) 
          else
            Map.get_and_update(result, :b , fn x -> x + 1 end)
          end
        end)
    end)
  end

  def nodeBef(node) do
    matrix = doGraph()
    length = node - 2
    matrix
    |>Enum.at(node)
    |>Enum.slice(0..length)
  end 

  def nodeAft(node) do
    matrix = doGraph()
    matrix
    |>Enum.at(node)
    |>Enum.slice(node..100)
  end 

  ############################
  #         Parte 1          #
  ############################       
  def graph(matrix) do
    #(3, 1, [[1,1],[0,2],[0,2]])
    #(3, 1, [[p0: 1, A: 1],[p1: 0, A: 2],[p2: 0, A: 2]]) ------
    #(3, 1, [["p0", 1, "A", 1],["p1", 0, "A", 2],["p2", 0, "A", 2]])
    
    matrix = List.flatten(matrix)
    #La transicion, la posición indica cuál es la transicion que quieres
    #t = Enum.at(matrix, 1)
    #El nodo, la posicion indica el nodo que quieres
    #m = Enum.at(matrix, 0)
    #Probamos si la transicón esta habilitada

    fire(matrix, 1, 1)
  end
  
  # M es tu nodo actual - Por posicion de una matriz en columna
  # T es la transcición por la cual pasar  - Por posicionde una matriz en fila
  # Return : M nodos finales con token
  def fire(net, m, t) do
  #([p0: 1, A: 1,p1: 0, A: 2,p2: 0, A: 2], 1, 1)
    net
    |>List.flatten()
    |>Enum.chunk_every(2) #Numero de transciciones + 1
    
    # currentNode = Enum.at(net, 0)
    # currentNode = Tuple.delete_at(currentNode,m)
    # currentNode = Tuple.insert_at(currentNode,m, 0)
    # currentNode = Tuple.to_list(currentNode)
    net
    |>Enum.at(0)
    |>Tuple.delete_at(m)
    |>Tuple.insert_at(m, 0)
    |>Tuple.to_list()
    
    net 
    |>Enum.to_list()
    |>List.delete_at(0)
    |>List.delete_at(0)
    |>Enum.chunk_every(2)
    |>Enum.map(fn x -> nextNode(x) end)

  end

  def nextNode(node) do
    node
    |>Enum.at(0)
    |>Tuple.delete_at(1)
    |>Tuple.insert_at(1, 1)
    |>Tuple.to_list()
  end

  def toMap(matrix) do
    map = List.flatten(matrix)
    map = Enum.chunk_every(map, 2)
    map = Enum.into(map, %{}, fn [a, b] -> {a, b} end)
    Map.values(map)
  end

  def isToken(node) do
    map =  Enum.chunk_every(node, 2)
    map =  Enum.take_every(map,2)
    map =  Enum.into(map, %{}, fn [a, b] -> {a, b} end)
    Map.values(map)
  end

  def run(graph) do
    List.to_string(graph)
  end
end
